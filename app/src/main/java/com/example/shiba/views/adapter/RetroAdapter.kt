package com.example.shiba.views.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.shiba.databinding.ListItemBinding
import com.example.shiba.model.models.Shiba

class RetroAdapter : RecyclerView.Adapter<RetroAdapter.RetroViewHolder>() {

    private lateinit var data : List<Shiba>

    class RetroViewHolder(private val binding : ListItemBinding) : RecyclerView.ViewHolder(binding.root) {
            fun applyToItem(item:String) {
                binding.shibeImage.loadImage(item)
            }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RetroViewHolder {
       val binding = ListItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RetroViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RetroViewHolder, position: Int) {
    val item = data[position]
        holder.applyToItem(item.url)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun applyData(data: List<Shiba>) {
        this.data = data
    }

}
fun ImageView.loadImage(url : String) {
    Glide.with(context).load(url).into(this)
}