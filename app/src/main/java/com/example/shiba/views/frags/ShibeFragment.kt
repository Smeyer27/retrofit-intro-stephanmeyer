package com.example.shiba.views.frags

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.example.shiba.databinding.FragmentShibeBinding
import com.example.shiba.model.repo.RetroRepo
import com.example.shiba.model.util.Resource
import com.example.shiba.viewmodel.ShibaViewModel
import com.example.shiba.viewmodel.ShibaViewModelFactory
import com.example.shiba.views.adapter.RetroAdapter
import kotlinx.coroutines.flow.collectLatest

class ShibeFragment : Fragment() {
    private var _binding: FragmentShibeBinding? = null
    private val binding: FragmentShibeBinding get() = _binding!!
    private val repo by lazy { RetroRepo(requireContext()) }
    private val viewModel by activityViewModels<ShibaViewModel>() { ShibaViewModelFactory(repo) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShibeBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(binding) {
        shibeRecyclerView.layoutManager = GridLayoutManager(context, 2)
        with(viewModel) {
            lifecycleScope.launchWhenCreated {
                liveData.collectLatest { resourceValue ->
                    when (resourceValue) {
                        is Resource.Loading -> {

                        }
                        is Resource.Error -> {

                        }
                        is Resource.Success -> {
                            shibeRecyclerView.adapter = RetroAdapter().apply {
                                applyData(resourceValue.data)
                            }
                        }
                    }
                }
            }
        }
    }
}