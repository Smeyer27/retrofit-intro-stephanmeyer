package com.example.shiba.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.shiba.model.repo.RetroRepo

class ShibaViewModelFactory (private val repo: RetroRepo) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ShibaViewModel(repo) as T
    }
}