package com.example.shiba.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.shiba.model.models.Shiba
import com.example.shiba.model.repo.RetroRepo
import com.example.shiba.model.util.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class ShibaViewModel (private val repo : RetroRepo): ViewModel() {

    private val _liveData = MutableStateFlow<Resource<List<Shiba>>>(Resource.Loading())
            val liveData : StateFlow<Resource<List<Shiba>>> get() = _liveData

    init{
        viewModelScope.launch(Dispatchers.Main) {
            //connect to Repo
            _liveData.emit(repo.getShibas())
        }
    }
}