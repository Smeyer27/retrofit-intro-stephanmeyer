package com.example.shiba.model.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "its_shibas")
data class Shiba(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val url : String
)
