package com.example.shiba.model.repo

import android.content.Context
import com.example.shiba.model.models.Shiba
import com.example.shiba.model.util.Resource
import com.example.shiba.room.ShibaDB
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RetroRepo(val context: Context) {

    private val retroApiService by lazy { RetroApi.retrofitInstance }

    private val shibaDao = ShibaDB.getDatabaseInstance(context).shibaDao()


    suspend fun getShibas() : Resource<List<Shiba>> = withContext(Dispatchers.IO) {
        return@withContext try{
            val response = retroApiService.getShibas()
            val dbInfo = shibaDao.getShibas()
            if(response.isSuccessful && (response.body())!!.size > dbInfo.size){
                val shibas = response.body()!!.map { Shiba(url = it) }
                shibaDao.addShibas(shibas)
                Resource.Success(shibaDao.getShibas())
            }
            else{
                Resource.Success(dbInfo)
            }
        }
        catch(e: Exception){
            Resource.Error(e.localizedMessage.toString())
        }
    }
}