package com.example.shiba.model.repo

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface RetroApi {
    @GET("/api/shibes")

    suspend fun getShibas(@Query("count") count : Int = 50) : Response<List<String>>

    companion object {
        val retrofitInstance by lazy {
            Retrofit
                .Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://shibe.online")
                .build()
                .create(RetroApi::class.java)
        }
    }
}