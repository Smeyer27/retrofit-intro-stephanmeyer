package com.example.shiba.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.shiba.model.models.Shiba

@Dao
interface ShibaDAO {

    @Query("SELECT*FROM its_shibas")
    fun getShibas() : List<Shiba>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addShibas (shibas : List<Shiba>)
}