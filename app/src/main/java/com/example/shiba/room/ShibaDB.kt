package com.example.shiba.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.shiba.model.models.Shiba

@Database(entities = [Shiba::class], exportSchema = false, version = 9000)
abstract class ShibaDB : RoomDatabase() {

    abstract fun shibaDao() : ShibaDAO

    companion object {
        private const val DB_NAME = "SHIBA_DATABASE"
        fun getDatabaseInstance(context: Context) : ShibaDB {
            return Room.databaseBuilder(context, ShibaDB::class.java, DB_NAME)
                .fallbackToDestructiveMigration().build()
        }
    }
}